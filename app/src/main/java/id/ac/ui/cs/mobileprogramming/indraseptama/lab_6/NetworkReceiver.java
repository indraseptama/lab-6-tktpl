package id.ac.ui.cs.mobileprogramming.indraseptama.lab_6;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

public class NetworkReceiver extends BroadcastReceiver {
    final String networkSSID = "Barraza";
    final String networkPass = "1sampai8";
    private WifiManager wifiManager;
    private int netId;
    private static final String TAG = "NetworkReceiver";
    private String ssid;
    private Context currContext;

    @Override
    public void onReceive(final Context context, Intent intent) {
        currContext = context;
        ConnectivityManager connectivityManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        wifiManager = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
        WifiConfiguration conf = new WifiConfiguration();

        conf.SSID = String.format("\"%s\"", networkSSID);
        conf.preSharedKey = String.format("\"%s\"", networkPass);

        WifiInfo wifiInfo = wifiManager.getConnectionInfo();
        ssid = wifiInfo.getSSID();

        netId = wifiManager.addNetwork(conf);
        if(networkInfo != null){
            boolean isWifiAvailable = connectivityManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI).isConnected();
            if(isWifiAvailable){
                if(!ssid.equals(String.format("\"%s\"", networkSSID)) && !ssid.equals("<unknown ssid>")){
                    new ChangeWifiToHotSpot(context).execute();
                }
            }
        }
        else{
            wifiManager.setWifiEnabled(true);
            new ConnectHotSpot(currContext).execute();
        }
    }

    private class ChangeWifiToHotSpot extends AsyncTask<Void, Void, Boolean>{

        public  ChangeWifiToHotSpot(Context context){
            Toast.makeText(context, "Disconnecting WIFI",
                    Toast.LENGTH_LONG).show();
        }
        @Override
        protected Boolean doInBackground(Void... voids) {
            return wifiManager.disconnect();
        }

        @Override
        protected void onPostExecute(Boolean aBoolean) {
            new ConnectHotSpot(currContext).execute();
        }
    }

    private class ConnectHotSpot extends AsyncTask<Void, Void, Boolean>{

        public ConnectHotSpot(Context context){
            Toast.makeText(context, "Connecting to "+networkSSID+" with password: "+networkPass,
                    Toast.LENGTH_LONG).show();
        }

        @Override
        protected Boolean doInBackground(Void... voids) {
            wifiManager.enableNetwork(netId, true);
            return wifiManager.reconnect();
        }
    }
}
